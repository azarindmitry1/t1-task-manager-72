package ru.t1.dazarin.tm.unit.repository;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import ru.t1.dazarin.tm.api.repository.ProjectDtoRepository;
import ru.t1.dazarin.tm.configuration.DataBaseConfiguration;
import ru.t1.dazarin.tm.configuration.WebApplicationConfiguration;
import ru.t1.dazarin.tm.marker.UnitCategory;
import ru.t1.dazarin.tm.model.dto.ProjectDto;

import javax.transaction.Transactional;
import java.util.Collections;
import java.util.UUID;

@Transactional
@WebAppConfiguration
@Category(UnitCategory.class)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {DataBaseConfiguration.class, WebApplicationConfiguration.class})
public class ProjectRepositoryTest {

    @NotNull
    private static final String USER_ID = UUID.randomUUID().toString();

    @Autowired
    private ProjectDtoRepository projectDtoRepository;

    @NotNull
    private ProjectDto projectOne;

    @Before
    public void setUp() {
        projectOne = new ProjectDto();
        projectOne.setName("Project One");
        projectOne.setDescription("Project One Description");
        projectOne.setUserId(USER_ID);
        projectDtoRepository.save(projectOne);
    }

    @After
    public void tearDown() {
        projectDtoRepository.delete(projectOne);
    }

    @Test
    public void findByUserIdAndId() {
        Assert.assertNull(projectDtoRepository.findByUserIdAndId(null, projectOne.getId()));
        Assert.assertNull(projectDtoRepository.findByUserIdAndId(USER_ID, null));
        Assert.assertNull(projectDtoRepository.findByUserIdAndId(null, null));
        Assert.assertNotNull(projectDtoRepository.findByUserIdAndId(USER_ID, projectOne.getId()));
    }

    @Test
    public void findAllByUserId() {
        Assert.assertNotEquals(Collections.emptyList(), projectDtoRepository.findAllByUserId(USER_ID));
        Assert.assertEquals(Collections.emptyList(), projectDtoRepository.findAllByUserId(null));
        Assert.assertNotNull(projectDtoRepository.findAllByUserId(USER_ID));
    }

    @Test
    public void deleteByUserIdAndId() {
        Assert.assertNotEquals(Collections.emptyList(), projectDtoRepository.findAllByUserId(USER_ID));
        Assert.assertEquals(Collections.emptyList(), projectDtoRepository.findAllByUserId(null));
        projectDtoRepository.deleteByUserIdAndId(USER_ID, projectOne.getId());
        Assert.assertNotNull(projectDtoRepository.findAllByUserId(USER_ID));
        Assert.assertEquals(Collections.emptyList(), projectDtoRepository.findAllByUserId(USER_ID));
    }

    @Test
    public void deleteByUserId() {
        Assert.assertNotNull(projectDtoRepository.findAllByUserId(USER_ID));
        Assert.assertNotEquals(Collections.emptyList(), projectDtoRepository.findAllByUserId(USER_ID));
        Assert.assertEquals(Collections.emptyList(), projectDtoRepository.findAllByUserId(null));
        projectDtoRepository.deleteByUserIdAndId(USER_ID, projectOne.getId());
        Assert.assertEquals(Collections.emptyList(), projectDtoRepository.findAllByUserId(USER_ID));
    }

}
