package ru.t1.dazarin.tm.unit.repository;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import ru.t1.dazarin.tm.api.repository.TaskDtoRepository;
import ru.t1.dazarin.tm.configuration.DataBaseConfiguration;
import ru.t1.dazarin.tm.configuration.WebApplicationConfiguration;
import ru.t1.dazarin.tm.marker.UnitCategory;
import ru.t1.dazarin.tm.model.dto.TaskDto;

import javax.transaction.Transactional;
import java.util.Collections;
import java.util.UUID;

@Transactional
@WebAppConfiguration
@Category(UnitCategory.class)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {DataBaseConfiguration.class, WebApplicationConfiguration.class})
public class TaskRepositoryTest {

    @NotNull
    private static final String USER_ID = UUID.randomUUID().toString();

    @Autowired
    private TaskDtoRepository taskDtoRepository;

    @NotNull
    private TaskDto taskOne;

    @Before
    public void setUp() {
        taskOne = new TaskDto();
        taskOne.setName("Task One");
        taskOne.setDescription("Task One Description");
        taskOne.setUserId(USER_ID);
        taskDtoRepository.save(taskOne);
    }

    @After
    public void tearDown() {
        taskDtoRepository.delete(taskOne);
    }

    @Test
    public void findByUserIdAndId() {
        Assert.assertNull(taskDtoRepository.findByUserIdAndId(null, taskOne.getId()));
        Assert.assertNull(taskDtoRepository.findByUserIdAndId(USER_ID, null));
        Assert.assertNull(taskDtoRepository.findByUserIdAndId(null, null));
        Assert.assertNotNull(taskDtoRepository.findByUserIdAndId(USER_ID, taskOne.getId()));
    }

    @Test
    public void findAllByUserId() {
        Assert.assertNotEquals(Collections.emptyList(), taskDtoRepository.findAllByUserId(USER_ID));
        Assert.assertEquals(Collections.emptyList(), taskDtoRepository.findAllByUserId(null));
        Assert.assertNotNull(taskDtoRepository.findAllByUserId(USER_ID));
    }

    @Test
    public void deleteByUserIdAndId() {
        Assert.assertNotEquals(Collections.emptyList(), taskDtoRepository.findAllByUserId(USER_ID));
        Assert.assertEquals(Collections.emptyList(), taskDtoRepository.findAllByUserId(null));
        taskDtoRepository.deleteByUserIdAndId(USER_ID, taskOne.getId());
        Assert.assertNotNull(taskDtoRepository.findAllByUserId(USER_ID));
        Assert.assertEquals(Collections.emptyList(), taskDtoRepository.findAllByUserId(USER_ID));
    }

    @Test
    public void deleteByUserId() {
        Assert.assertNotNull(taskDtoRepository.findAllByUserId(USER_ID));
        Assert.assertNotEquals(Collections.emptyList(), taskDtoRepository.findAllByUserId(USER_ID));
        Assert.assertEquals(Collections.emptyList(), taskDtoRepository.findAllByUserId(null));
        taskDtoRepository.deleteByUserIdAndId(USER_ID, taskOne.getId());
        Assert.assertEquals(Collections.emptyList(), taskDtoRepository.findAllByUserId(USER_ID));
    }

}
