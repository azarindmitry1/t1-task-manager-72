package ru.t1.dazarin.tm.constant;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;
import ru.t1.dazarin.tm.enumerated.RoleType;

@UtilityClass
public class UserTestData {

    @NotNull
    public final static String TEST_USER_LOGIN = "test_user";

    @NotNull
    public final static String TEST_USER_PASSWORD = "test_user";

    @NotNull
    public final static RoleType USER_ROLE_TYPE = RoleType.USER;

}
