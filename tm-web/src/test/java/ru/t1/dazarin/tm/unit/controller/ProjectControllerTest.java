package ru.t1.dazarin.tm.unit.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ru.t1.dazarin.tm.configuration.DataBaseConfiguration;
import ru.t1.dazarin.tm.configuration.WebApplicationConfiguration;
import ru.t1.dazarin.tm.marker.UnitCategory;
import ru.t1.dazarin.tm.model.dto.ProjectDto;
import ru.t1.dazarin.tm.service.dto.ProjectDtoService;
import ru.t1.dazarin.tm.util.UserUtil;

import javax.transaction.Transactional;
import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Transactional
@WebAppConfiguration
@Category(UnitCategory.class)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {DataBaseConfiguration.class, WebApplicationConfiguration.class})
public class ProjectControllerTest {

    @NotNull
    private final static String PROJECT_API_URL = "http://localhost:8080/project/";

    @NotNull
    private String userId;

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @NotNull
    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @NotNull
    private ProjectDto projectOne;

    @NotNull
    @Autowired
    private ProjectDtoService projectDtoService;

    @Before
    public void setUp() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
        @NotNull final UsernamePasswordAuthenticationToken token =
                new UsernamePasswordAuthenticationToken("admin", "admin");
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        userId = UserUtil.getUserId();
        projectOne = new ProjectDto();
        projectOne.setName("Project One");
        projectOne.setDescription("Project One Description");
        projectDtoService.save(userId, projectOne);
    }

    @After
    public void tearDown() {
        projectDtoService.deleteAll(userId);
    }

    @Test
    @SneakyThrows
    public void create() {
        @NotNull final String url = PROJECT_API_URL + "create";
        mockMvc.perform(MockMvcRequestBuilders.post(url)).andDo(print()).andExpect(status().is3xxRedirection());
        @Nullable final List<ProjectDto> projects = projectDtoService.findAll(userId);
        Assert.assertNotNull(projects);
        Assert.assertEquals(2, projects.size());
    }

    @Test
    @SneakyThrows
    public void delete() {
        @NotNull final String projectId = projectOne.getId();
        @NotNull final String url = PROJECT_API_URL + "delete/" + projectId;
        mockMvc.perform(MockMvcRequestBuilders.get(url)).andDo(print()).andExpect(status().is3xxRedirection());
        Assert.assertNull(projectDtoService.findById(userId, projectId));
    }

    @Test
    @SneakyThrows
    public void list() {
        @NotNull final String url = "http://localhost:8080/projects";
        mockMvc.perform(MockMvcRequestBuilders.get(url)).andDo(print()).andExpect(status().isOk());
    }

    @Test
    @SneakyThrows
    public void update() {
        @NotNull final String projectId = projectOne.getId();
        @NotNull final String url = PROJECT_API_URL + "update/" + projectId;
        mockMvc.perform(MockMvcRequestBuilders.get(url)).andDo(print()).andExpect(status().is2xxSuccessful());
    }

    @Test
    @SneakyThrows
    public void updatePostMethod() {
        @NotNull final String projectId = projectOne.getId();
        @NotNull final String url = PROJECT_API_URL + "update/" + projectId;
        @NotNull final String json = new ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(projectOne);
        mockMvc.perform(MockMvcRequestBuilders.post(url).content(json)).andDo(print()).andExpect(status().is3xxRedirection());
    }

}
