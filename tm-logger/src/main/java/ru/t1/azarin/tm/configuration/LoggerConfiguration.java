package ru.t1.azarin.tm.configuration;

import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import javax.jms.ConnectionFactory;

@Configuration
@ComponentScan("ru.t1.azarin.tm")
public class LoggerConfiguration {

    @NotNull
    private static final String URL = ActiveMQConnection.DEFAULT_BROKER_URL;

    @Bean
    public ConnectionFactory factory() {
        @NotNull final ActiveMQConnectionFactory factory = new ActiveMQConnectionFactory(URL);
        return factory;
    }

}
