package ru.t1.dazarin.tm.service.dto;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.dazarin.tm.api.repository.TaskDtoRepository;
import ru.t1.dazarin.tm.exception.entity.TaskNotFoundException;
import ru.t1.dazarin.tm.exception.field.TaskIdEmptyException;
import ru.t1.dazarin.tm.exception.field.UserIdEmptyException;
import ru.t1.dazarin.tm.model.dto.TaskDto;

import javax.transaction.Transactional;
import java.util.List;

@Service
@NoArgsConstructor
public class TaskDtoService {

    @Autowired
    private TaskDtoRepository taskDtoRepository;

    public List<TaskDto> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return taskDtoRepository.findAllByUserId(userId);
    }

    @Nullable
    public TaskDto findById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new TaskIdEmptyException();
        return taskDtoRepository.findByUserIdAndId(userId, id);
    }

    @Transactional
    public TaskDto create(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @Nullable final TaskDto taskDto = new TaskDto();
        taskDto.setName("Task Name " + System.currentTimeMillis());
        taskDto.setDescription("Task Description");
        taskDto.setUserId(userId);
        return taskDtoRepository.saveAndFlush(taskDto);
    }

    @Transactional
    public void deleteById(@Nullable final String userId, @Nullable final String taskDtoId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (taskDtoId == null || taskDtoId.isEmpty()) throw new TaskIdEmptyException();
        if (!taskDtoRepository.existsById(taskDtoId)) throw new TaskNotFoundException();
        taskDtoRepository.deleteByUserIdAndId(userId, taskDtoId);
    }

    @Transactional
    public void deleteAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        taskDtoRepository.deleteByUserId(userId);
    }

    @Transactional
    public TaskDto save(@Nullable final String userId, @Nullable final TaskDto taskDto) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (taskDto == null) throw new TaskNotFoundException();
        taskDto.setUserId(userId);
        return taskDtoRepository.save(taskDto);
    }

}
