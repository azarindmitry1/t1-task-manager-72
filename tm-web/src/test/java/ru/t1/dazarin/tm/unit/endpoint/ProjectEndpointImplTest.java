package ru.t1.dazarin.tm.unit.endpoint;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ru.t1.dazarin.tm.configuration.DataBaseConfiguration;
import ru.t1.dazarin.tm.configuration.WebApplicationConfiguration;
import ru.t1.dazarin.tm.marker.UnitCategory;
import ru.t1.dazarin.tm.model.dto.ProjectDto;
import ru.t1.dazarin.tm.service.dto.ProjectDtoService;
import ru.t1.dazarin.tm.util.UserUtil;

import javax.transaction.Transactional;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Transactional
@WebAppConfiguration
@Category(UnitCategory.class)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {DataBaseConfiguration.class, WebApplicationConfiguration.class})
public class ProjectEndpointImplTest {

    @NotNull
    private final static String PROJECT_API_URL = "http://localhost:8080/api/project/";

    @NotNull
    private String userId;

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @NotNull
    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @NotNull
    private ProjectDto project = new ProjectDto();

    @NotNull
    @Autowired
    private ProjectDtoService projectDtoService;

    @Before
    public void setUp() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
        @NotNull final UsernamePasswordAuthenticationToken token =
                new UsernamePasswordAuthenticationToken("admin", "admin");
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        userId = UserUtil.getUserId();
        project.setUserId(userId);
        updateMock(project);
    }

    @After
    @SneakyThrows
    public void tearDown() {
        mockMvc.perform(
                MockMvcRequestBuilders.delete(PROJECT_API_URL + "deleteAll")
                        .contentType(MediaType.APPLICATION_JSON)
        ).andDo(print()).andExpect(status().isOk());
    }

    @SneakyThrows
    private ProjectDto findByIdMock(@NotNull final String id) {
        @NotNull final String findByIdUrl = PROJECT_API_URL + "findById/" + id;
        @NotNull final String json = mockMvc.perform(MockMvcRequestBuilders.get(findByIdUrl)
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        @NotNull final ObjectMapper mapper = new XmlMapper();
        if (json.isEmpty()) return null;
        return mapper.readValue(json, ProjectDto.class);
    }

    @Test
    public void findById() {
        @NotNull final String projectId = project.getId();
        @Nullable final ProjectDto projectDto = findByIdMock(projectId);
        Assert.assertNotNull(projectDto);
        Assert.assertEquals(projectId, projectDto.getId());
    }

    @SneakyThrows
    private List<ProjectDto> findAllMock() {
        @NotNull final String existsByIdUrl = PROJECT_API_URL + "findAll";
        @NotNull final String json = mockMvc.perform(MockMvcRequestBuilders.get(existsByIdUrl)
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        @NotNull final ObjectMapper mapper = new XmlMapper();
        return Arrays.asList(mapper.readValue(json, ProjectDto[].class));
    }

    @Test
    public void findAll() {
        @NotNull final ProjectDto projectById = findByIdMock(project.getId());
        @NotNull final List<ProjectDto> projects = findAllMock();
        @NotNull final ProjectDto projectDto = projects.get(0);
        Assert.assertEquals(projectById.getId(), projectDto.getId());
    }

    @SneakyThrows
    private void updateMock(@NotNull final ProjectDto projectDto) {
        @NotNull final String url = PROJECT_API_URL + "update";
        @NotNull final ObjectMapper mapper = new ObjectMapper();
        @NotNull final String json = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(projectDto);
        mockMvc.perform(
                MockMvcRequestBuilders
                        .put(url)
                        .content(json)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print()).andExpect(status().isOk()
        );
    }

    @Test
    public void update() {
        @NotNull final ProjectDto projectDto = new ProjectDto();
        projectDto.setName("TEST");
        projectDto.setUserId(userId);
        @NotNull final String projectId = projectDto.getId();
        updateMock(projectDto);
        ProjectDto projectById = findByIdMock(projectId);
        Assert.assertNotNull(projectById);
        Assert.assertEquals(projectId, projectById.getId());
        Assert.assertEquals("TEST", projectById.getName());
        Assert.assertEquals(userId, projectById.getUserId());
    }

    @SneakyThrows
    private void deleteByIdMock(@NotNull final String id) {
        mockMvc.perform(
                MockMvcRequestBuilders.delete(PROJECT_API_URL + "deleteById/" + id)
                        .contentType(MediaType.APPLICATION_JSON)
        ).andDo(print()).andExpect(status().isOk());
    }

    @Test
    public void deleteById() {
        @NotNull final String projectId = project.getId();
        @Nullable ProjectDto projectDto = findByIdMock(projectId);
        Assert.assertNotNull(projectDto);
        Assert.assertEquals(projectId, projectDto.getId());
        deleteByIdMock(projectId);
        projectDto = findByIdMock(projectId);
        Assert.assertNull(projectDto);
    }

    @SneakyThrows
    private void deleteAllMock(@NotNull final List<ProjectDto> projects) {
        @NotNull final ObjectMapper mapper = new ObjectMapper();
        @NotNull final String json = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(projects);
        mockMvc.perform(
                MockMvcRequestBuilders.delete(PROJECT_API_URL + "deleteAll")
                        .content(json)
                        .contentType(MediaType.APPLICATION_JSON)
        ).andDo(print()).andExpect(status().isOk());
    }

    @Test
    public void deleteAll() {
        @NotNull final String projectId = project.getId();
        @Nullable ProjectDto projectDto = findByIdMock(projectId);
        Assert.assertNotNull(projectDto);
        Assert.assertEquals(projectId, projectDto.getId());
        List<ProjectDto> projects = new ArrayList<>();
        projects.add(projectDto);
        deleteAllMock(projects);
        projectDto = findByIdMock(projectId);
        Assert.assertNull(projectDto);
    }

}
