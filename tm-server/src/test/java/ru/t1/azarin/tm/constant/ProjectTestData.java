package ru.t1.azarin.tm.constant;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;
import ru.t1.azarin.tm.dto.model.ProjectDto;
import ru.t1.azarin.tm.enumerated.Status;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@UtilityClass
public final class ProjectTestData {

    @NotNull
    public final static ProjectDto USER1_PROJECT1 = new ProjectDto();

    @NotNull
    public final static ProjectDto USER1_PROJECT2 = new ProjectDto();

    @NotNull
    public final static ProjectDto ADMIN_PROJECT1 = new ProjectDto();

    @NotNull
    public final static ProjectDto ADMIN_PROJECT2 = new ProjectDto();

    @NotNull
    public final static List<ProjectDto> USER1_PROJECT_LIST = Arrays.asList(USER1_PROJECT1, USER1_PROJECT2);

    @NotNull
    public final static List<ProjectDto> ADMIN_PROJECT_LIST = Arrays.asList(ADMIN_PROJECT1, ADMIN_PROJECT2);

    @NotNull
    public final static List<ProjectDto> PROJECT_LIST = new ArrayList<>();

    static {
        PROJECT_LIST.addAll(USER1_PROJECT_LIST);
        PROJECT_LIST.addAll(ADMIN_PROJECT_LIST);

        for (int i = 0; i < PROJECT_LIST.size(); i++) {
            @NotNull final ProjectDto project = PROJECT_LIST.get(i);
            project.setName("project-" + i);
            project.setDescription("description-" + i);
            project.setStatus(Status.NOT_STARTED);
        }
    }

}
