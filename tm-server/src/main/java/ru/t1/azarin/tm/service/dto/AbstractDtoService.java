package ru.t1.azarin.tm.service.dto;

import lombok.NoArgsConstructor;
import org.springframework.stereotype.Service;
import ru.t1.azarin.tm.api.service.dto.IDtoService;
import ru.t1.azarin.tm.dto.model.AbstractDtoModel;

@Service
@NoArgsConstructor
public abstract class AbstractDtoService<M extends AbstractDtoModel> implements IDtoService<M> {

}
