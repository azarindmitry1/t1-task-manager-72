package ru.t1.dazarin.tm.service.dto;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.dazarin.tm.api.repository.ProjectDtoRepository;
import ru.t1.dazarin.tm.exception.entity.ProjectNotFoundException;
import ru.t1.dazarin.tm.exception.field.ProjectIdEmptyException;
import ru.t1.dazarin.tm.exception.field.UserIdEmptyException;
import ru.t1.dazarin.tm.model.dto.ProjectDto;

import javax.transaction.Transactional;
import java.util.List;

@Service
@NoArgsConstructor
public class ProjectDtoService {

    @Autowired
    private ProjectDtoRepository projectDtoRepository;

    public List<ProjectDto> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return projectDtoRepository.findAllByUserId(userId);
    }

    @Nullable
    public ProjectDto findById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new ProjectIdEmptyException();
        return projectDtoRepository.findByUserIdAndId(userId, id);
    }

    @Transactional
    public ProjectDto create(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @Nullable final ProjectDto projectDto = new ProjectDto();
        projectDto.setName("Project Name " + System.currentTimeMillis());
        projectDto.setDescription("Project Description");
        projectDto.setUserId(userId);
        return projectDtoRepository.saveAndFlush(projectDto);
    }

    @Transactional
    public void deleteById(@Nullable final String userId, @Nullable final String projectDtoId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectDtoId == null || projectDtoId.isEmpty()) throw new ProjectIdEmptyException();
        if (!projectDtoRepository.existsById(projectDtoId)) throw new ProjectNotFoundException();
        projectDtoRepository.deleteByUserIdAndId(userId, projectDtoId);
    }

    @Transactional
    public void deleteAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        projectDtoRepository.deleteByUserId(userId);
    }

    @Transactional
    public ProjectDto save(@Nullable final String userId, @Nullable final ProjectDto projectDto) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectDto == null) throw new ProjectNotFoundException();
        projectDto.setUserId(userId);
        return projectDtoRepository.save(projectDto);
    }

}