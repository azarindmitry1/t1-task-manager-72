package ru.t1.azarin.tm.dto.response.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.azarin.tm.dto.model.UserDto;
import ru.t1.azarin.tm.dto.response.AbstractResponse;

@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractUserResponse extends AbstractResponse {

    @Nullable
    private UserDto user;

    public AbstractUserResponse(@Nullable final UserDto user) {
        this.user = user;
    }

}
