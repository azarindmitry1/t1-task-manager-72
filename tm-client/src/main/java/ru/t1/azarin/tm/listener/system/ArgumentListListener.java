package ru.t1.azarin.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.azarin.tm.api.model.ICommand;
import ru.t1.azarin.tm.event.ConsoleEvent;
import ru.t1.azarin.tm.listener.AbstractListener;

import java.util.Collection;

@Component
public final class ArgumentListListener extends AbstractSystemListener {

    @NotNull
    public final static String NAME = "arguments";

    @NotNull
    public final static String ARGUMENT = "-arg";

    @NotNull
    public final static String DESCRIPTION = "Display arguments of application.";

    @Override
    @EventListener(condition = "@argumentListListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[ARGUMENTS]");
        @NotNull final Collection<AbstractListener> commands = getListeners();
        for (@NotNull ICommand command : commands) {
            @Nullable final String argument = command.getArgument();
            if (argument == null || argument.isEmpty()) continue;
            System.out.println(argument);
        }
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }
}
