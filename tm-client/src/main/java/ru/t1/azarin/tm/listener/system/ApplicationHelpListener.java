package ru.t1.azarin.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.azarin.tm.api.model.ICommand;
import ru.t1.azarin.tm.event.ConsoleEvent;
import ru.t1.azarin.tm.listener.AbstractListener;

import java.util.Collection;

@Component
public final class ApplicationHelpListener extends AbstractSystemListener {

    @NotNull
    public final static String NAME = "help";

    @NotNull
    public final static String ARGUMENT = "-h";

    @NotNull
    public final static String DESCRIPTION = "Display application command.";

    @Override
    @EventListener(condition = "@applicationHelpListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[HELP]");
        @NotNull final Collection<AbstractListener> commands = getListeners();
        for (@NotNull final ICommand command : commands) System.out.println(command);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }
}
