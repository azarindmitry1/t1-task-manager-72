package ru.t1.dazarin.tm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.t1.dazarin.tm.enumerated.Status;
import ru.t1.dazarin.tm.model.dto.ProjectDto;
import ru.t1.dazarin.tm.service.dto.ProjectDtoService;
import ru.t1.dazarin.tm.util.UserUtil;

@Controller
public class ProjectController {

    @Autowired
    private ProjectDtoService projectDtoService;

    @PostMapping(value = "/project/create")
    public String create() {
        projectDtoService.create(UserUtil.getUserId());
        return "redirect:/projects";
    }

    @GetMapping(value = "/project/delete/{id}")
    public String delete(@PathVariable("id") String id) {
        projectDtoService.deleteById(UserUtil.getUserId(), id);
        return "redirect:/projects";
    }

    @GetMapping(value = "/project/update/{id}")
    public ModelAndView update(@PathVariable("id") String id) {
        final ProjectDto projectDto = projectDtoService.findById(UserUtil.getUserId(), id);
        final ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("project-update");
        modelAndView.addObject("project", projectDto);
        modelAndView.addObject("statuses", Status.values());
        return modelAndView;
    }

    @PostMapping(value = "/project/update/{id}")
    public String update(@ModelAttribute("project") ProjectDto project, BindingResult result) {
        projectDtoService.save(UserUtil.getUserId(), project);
        return "redirect:/projects";
    }

    @GetMapping(value = "/projects")
    public ModelAndView index() {
        return new ModelAndView(
                "project-list", "projects", projectDtoService.findAll(UserUtil.getUserId())
        );
    }

}
