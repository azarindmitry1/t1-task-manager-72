package ru.t1.dazarin.tm.unit.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ru.t1.dazarin.tm.configuration.DataBaseConfiguration;
import ru.t1.dazarin.tm.configuration.WebApplicationConfiguration;
import ru.t1.dazarin.tm.marker.UnitCategory;
import ru.t1.dazarin.tm.model.dto.TaskDto;
import ru.t1.dazarin.tm.service.dto.TaskDtoService;
import ru.t1.dazarin.tm.util.UserUtil;

import javax.transaction.Transactional;
import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Transactional
@WebAppConfiguration
@Category(UnitCategory.class)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {DataBaseConfiguration.class, WebApplicationConfiguration.class})
public class TaskControllerTest {

    @NotNull
    private final static String TASK_API_URL = "http://localhost:8080/task/";

    @NotNull
    private String userId;

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @NotNull
    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @NotNull
    private TaskDto taskOne;

    @NotNull
    @Autowired
    private TaskDtoService taskDtoService;

    @Before
    public void setUp() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
        @NotNull final UsernamePasswordAuthenticationToken token =
                new UsernamePasswordAuthenticationToken("admin", "admin");
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        userId = UserUtil.getUserId();
        taskOne = new TaskDto();
        taskOne.setName("Task One");
        taskOne.setDescription("Task One Description");
        taskDtoService.save(userId, taskOne);
    }

    @After
    public void tearDown() {
        taskDtoService.deleteAll(userId);
    }

    @Test
    @SneakyThrows
    public void create() {
        @NotNull final String url = TASK_API_URL + "create";
        mockMvc.perform(MockMvcRequestBuilders.post(url)).andDo(print()).andExpect(status().is3xxRedirection());
        @Nullable final List<TaskDto> tasks = taskDtoService.findAll(userId);
        Assert.assertNotNull(tasks);
        Assert.assertEquals(2, tasks.size());
    }

    @Test
    @SneakyThrows
    public void delete() {
        @NotNull final String taskId = taskOne.getId();
        @NotNull final String url = TASK_API_URL + "delete/" + taskId;
        mockMvc.perform(MockMvcRequestBuilders.get(url)).andDo(print()).andExpect(status().is3xxRedirection());
        Assert.assertNull(taskDtoService.findById(userId, taskId));
    }

    @Test
    @SneakyThrows
    public void list() {
        @NotNull final String url = "http://localhost:8080/tasks";
        mockMvc.perform(MockMvcRequestBuilders.get(url)).andDo(print()).andExpect(status().isOk());
    }

    @Test
    @SneakyThrows
    public void update() {
        @NotNull final String taskId = taskOne.getId();
        @NotNull final String url = TASK_API_URL + "update/" + taskId;
        mockMvc.perform(MockMvcRequestBuilders.get(url)).andDo(print()).andExpect(status().is2xxSuccessful());
    }

    @Test
    @SneakyThrows
    public void updatePostMethod() {
        @NotNull final String taskId = taskOne.getId();
        @NotNull final String url = TASK_API_URL + "update/" + taskId;
        @NotNull final String json = new ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(taskOne);
        mockMvc.perform(MockMvcRequestBuilders.post(url).content(json)).andDo(print()).andExpect(status().is3xxRedirection());
    }

}
